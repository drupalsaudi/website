### Description :
Verify that …..

### Tested in:
- [ ] web
- [ ] IOS
- [ ] Android

/label ~"Test Case"
/label ~"Team::QA"
/label ~Android
/label ~IOS
/label ~"Status::ToDo"
/label ~"Regression test" <optional>

/assign @<yourself>
